const fs = require('fs')
const express = require('express')
const app = express()
const cors = require('cors')
const bodyParser = require('body-parser');
const port = 3000


let words = fs.readFileSync('OpenTaal-210G-basis-gekeurd.txt').toString().split('\n');
console.log("Num words: " + words.length);

let klinkers = "aeiou";

let dict = {};
for(let i = 0; i < words.length; i++)
{
    let word = words[i];
    let arr = dict;
    
    for(let j = 0; j < word.length; j++)
    {
        let char = word[j].toLowerCase()
        if(klinkers.indexOf(char) != -1) continue;
        if(arr[char] == null)
            arr[char] = {};
        arr = arr[char];
    }
    if(arr["matches"] == undefined)
        arr["matches"] = [];
    arr["matches"].push(word);
}
console.log("Created dictionary!");


let getMostLikely = (word, matches) => {
    let original = { a : 0, e : 0, i : 0, o : 0, u : 0, y : 0 };
    let bestMatch = null;
    let bestScore = Infinity;
    for(let i = 0; i < word.length; i++)
    {
        let char = word.charAt(i).toLowerCase();
        if(klinkers.indexOf(char) > -1)
            original[char]++
    }
    for(let i = 0; i < matches.length; i++)
    {
        let potential = matches[i];
        let scores =  { a : 0, e : 0, i : 0, o : 0, u : 0, y : 0 };
        for(let j = 0; j < potential.length; j++)
        {
            let char = potential.charAt(j).toLowerCase();
            if(klinkers.indexOf(char) > -1)
                scores[char]++
        }
        let score = getMatchingKlinkers(original, scores);
        if(score < bestScore)
        {
            bestMatch = potential;
            bestScore = score;
        }
    }
    // for(let )
    return bestMatch;
}

let getMatchingKlinkers = (a,b) => {
    let score = 0;
    for(let key in a)
    {
        score += Math.abs(a[key] - b[key]);
    }
    return score;
}

app.get('/find/:word', (request, response) => {
    response.setHeader('Content-Type', 'application/json');
    let word = request.params.word;
    console.log(" < " + word);
    
    let found = null;
    let dictRef = dict;
    for(let i = 0; i < word.length; i++)
    {
        let char = word.charAt(i).toLowerCase();
        if(klinkers.indexOf(char) != -1) continue;
        if(dictRef == null) break;
        dictRef = dictRef[char];
    }
    let matches = dictRef ? dictRef["matches"] : undefined;
    let res = {
        matches : matches,
        suggestion : matches != undefined ? getMostLikely(word, matches)  : null
    };
    console.log(" > " + JSON.stringify(res))
    response.send(JSON.stringify(res));
});


app.use(bodyParser.json())
app.listen(port, () => {
    console.log(`Running on port: ${port}`)
})
