# Klinkerloos API
Middels een HTTP GET request kan een woord opgegeven worden wat doorzocht wordt door een lijst met woorden van OpenTaal.org.   

Bijvoorbeeld:
```
http://localhost:3000/find/slapzak
```

Er wordt een lijst met matches teruggegeven waar dezelfde volgorde aan medeklinkers op van toepassing is. Klinkers worden genegeerd. Aan de hand van hoe vaak klinkers voorkomen wordt een suggestie gegeven voor een woord:
```json
{
    "matches" : [
        "slaapzak",
        "sleepzak"
    ],
    "suggestion" : "slaapzak"
}
```

## Installatie
Om de API te draaien dient Node.js geïnstalleerd te zijn. Om de bijbehorende modules te installeren kan via de commandline `npm install` gedraaid worden (vanuit de directory waar dit project in staat).

Om de API te starten:
```
node index.js
```
of
```
npm start
```